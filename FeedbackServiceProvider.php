<?php namespace Gesar\Feedback;

use Illuminate\Support\ServiceProvider;

class FeedbackServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if(! $this->app->routesAreCached()) {
            require __DIR__.'/routes.php';
        }

        $this->publishes([
            __DIR__ . '/migrations/' => base_path('/database/migrations'),
        ], 'migrations');

        $this->loadViewsFrom(__DIR__.'/views', 'feedback');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

//
    }

}