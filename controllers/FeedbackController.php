<?php namespace Gesar\Feedback\Controllers;

/**
 * Created by PhpStorm.
 * User: gesar
 * Date: 27.07.15
 * Time: 10:32
 */

use Gesar\Feedback;
use App\Http\Controllers\Controller;

class MapController extends Controller
{
    public function __construct(){

    }

    public function getIndex(){
        return view('feedback::form', ['title' => 'Форма обратной связи с возможностью выбора аптеки']);
    }
}