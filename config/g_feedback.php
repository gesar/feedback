<?
return [
    'fields' => [
        'name' => [
            'type' => 'text',
            'title' => 'Name',
            'required' => true
        ],
        'email' => [
            'type' => 'text',
            'title' => 'E-Mail',
            'required' => true
        ],
        'comment' => [
            'type' => 'textarea',
            'title' => 'Комментарий'
        ],
        'drugstore' => [
            'type' => 'text',
            'title' => 'Аптека',
            'list_model' => 'listModelClassAsString' // or listModelClassAs::class
        ]
    ]
];